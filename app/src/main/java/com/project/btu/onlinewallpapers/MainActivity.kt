package com.project.btu.onlinewallpapers

import android.content.ContentResolver
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log.d
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.Toast
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Tasks
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.OnProgressListener
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_main.*

import com.squareup.picasso.Picasso
import java.lang.Exception


class MainActivity : AppCompatActivity() {
    var mStorageRef = FirebaseStorage.getInstance().getReference("uploads")
    var mDatabaseRef = FirebaseDatabase.getInstance().getReference("uploads")
    val PICK_IMAGE_REQUEST = 1
    var mImageUri: Uri? = null
    lateinit var url: String
    lateinit var uploadIdTemp: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onClickChooseFileButton(view: View) {
        openFileChooser()
    }

    fun onClickUploadFileButton(view: View) {
        uploadFile()
    }
    fun onClickShowUploads(view: View)
    {
        openImagesActivity()
    }



    fun openFileChooser() {
        var intent = Intent()
        intent.setType("image/*")
        intent.setAction(Intent.ACTION_GET_CONTENT)
        startActivityForResult(intent, PICK_IMAGE_REQUEST)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
            && data != null && data.getData() != null
        ) {
            mImageUri = data.data

            Picasso.get()
                .load(mImageUri)
                .into(imageView)
        }


    }

    fun getFileExtension(uri: Uri): String {
        var cR = getContentResolver()
        var mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(cR.getType(uri))

    }

    fun uploadFile() {

        if (mImageUri != null) {
            var fileReference = mStorageRef.child(
                System.currentTimeMillis().toString()
                        + "." + getFileExtension(mImageUri as Uri)
            )

            fileReference.putFile(mImageUri as Uri)
                .addOnSuccessListener {
                    progressBar.progress = 0

                    fileReference.downloadUrl.addOnSuccessListener {
                        val uploadID = mDatabaseRef.push().key


                        Toast.makeText(this@MainActivity, "Upload successful", Toast.LENGTH_LONG).show()
                        val upload = Upload(editTextNameFile.text.toString().trim(), it.toString())



                        uploadID?.let {
                            mDatabaseRef.child(uploadID).setValue(upload)
                        }


                    }.addOnFailureListener {
                        Toast.makeText(this@MainActivity, "Failure", Toast.LENGTH_SHORT).show()
                        d("FailureDownloadUrl",it.toString())
                    }




                }.addOnFailureListener {
                    Toast.makeText(this@MainActivity, it.message, Toast.LENGTH_SHORT).show()

                }

                .addOnProgressListener {
                    var progress: Double = (100.0 * it.bytesTransferred / it.totalByteCount)
                    progressBar.progress = progress.toInt()
                }
        } else {
            Toast.makeText(this, "No File Selected", Toast.LENGTH_SHORT).show()
        }


    }
    fun openImagesActivity()
    {
        d("goToIntent","idk")
        val intent = Intent(this,ImageActivity::class.java)
        startActivity(intent)
    }



}
