package com.project.btu.onlinewallpapers

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_image.*
import android.support.v7.widget.LinearLayoutManager
import android.util.Log.d
import android.view.View
import android.widget.Toast
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.image_item.*

class ImageActivity : AppCompatActivity() {

    var mUploads = mutableListOf<Upload>()
    lateinit var mAdapter: ImageAdapter
    var mDatabaseRef = FirebaseDatabase.getInstance().getReference("uploads")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)


        recycleView.setHasFixedSize(true)
        recycleView.layoutManager = LinearLayoutManager(this)


        val changeListener = object : ValueEventListener {

            override fun onDataChange(p0: DataSnapshot) {

                for (postSanpshot: DataSnapshot in p0.children) {
                    val upload = postSanpshot.getValue(Upload::class.java)
                    upload?.let {
                        mUploads.add(upload)
                    }
                    mAdapter = ImageAdapter(this@ImageActivity, mUploads)
                    recycleView.adapter = mAdapter
                }

            }

            override fun onCancelled(p0: DatabaseError) {
                d("onCanceledLoad", "Load Item Canceled", p0.toException())
            }
        }
        mDatabaseRef.addValueEventListener(changeListener)


    }

}
