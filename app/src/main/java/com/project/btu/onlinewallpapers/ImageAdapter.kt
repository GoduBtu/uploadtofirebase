package com.project.btu.onlinewallpapers
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.image_item.view.*

import com.squareup.picasso.Picasso


class ImageAdapter(var context: Context, var uploads:List<Upload>) : RecyclerView.Adapter<ImageAdapter.ImageViewHolder>() {

    var mContext = context
    var mUploads = uploads
    override fun getItemCount(): Int {
        return uploads.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return ImageViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.image_item, parent, false))

    }



    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {

        val uploadCurrent = uploads[position]
        holder.textImageView.text = uploadCurrent.name
        Picasso.get()
            .load(uploadCurrent.imageUrl)
            .placeholder(R.mipmap.ic_launcher)
            .fit()
            .centerInside()
            .into(holder.imageViewUpload)
    }
    class ImageViewHolder(var view:View) : RecyclerView.ViewHolder(view) {
        val textImageView = view.imageViewName!!
        val imageViewUpload = view.imageViewUpload!!

    }

}